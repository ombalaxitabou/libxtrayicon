LIB_PATH = /usr/local/lib
INCL_PATH = /usr/local/include

INSTALL = /usr/bin/install

# program name
lib = xtrayicon
# different major versions are not compatible
major = 1
# minor versions are backward compatible
minor = 2
# don't forget to update version number in readme file

linkername = lib$(lib).so
soname = $(linkername).$(major)
realname = $(soname).$(minor)

short = xti
obj = $(short).o
header = $(short).h

CC = gcc

CFLAGS = -fpic -c -Wall

#debug
#CFLAGS += -g

#deps
XCB = xcb xcb-image xcb-icccm
CFLAGS += `pkg-config --cflags $(XCB)`
LDFLAGS += `pkg-config --libs $(XCB)`

help: commands

## commands: show accepted commands
commands:
	grep -E '^##' Makefile | sed -e 's/## //g'

## libxtrayicon.so.X.Y: compile libxtrayicon
$(realname): $(obj)
	$(CC) -shared -Wl,-soname,$(soname) -o $(realname) $(obj)  $(LDFLAGS)

$(obj): %.o: %.c
	$(CC) $(CFLAGS) -o $@ $<

## install: install xkeyboardicon to dir set in Makefile
install: $(bin)
	$(INSTALL) -d $(LIB_PATH)/
	$(INSTALL) $(realname) $(LIB_PATH)
	ln -sf $(realname) $(LIB_PATH)/$(soname)
	ln -sf $(soname) $(LIB_PATH)/$(linkername)
	ldconfig
	$(INSTALL) -d $(INCL_PATH)/
	$(INSTALL) $(header) $(INCL_PATH)

## uninstall: remove installed files
uninstall:
	rm -r -f $(LIB_PATH)/$(realname) $(LIB_PATH)/$(soname) $(LIB_PATH)/$(linkername) $(INCL_PATH)/$(header)

## clean: remove compiled files (and not the installed ones)
clean:
	rm -f xti.o $(realname)

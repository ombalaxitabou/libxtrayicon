/** @file
 * libxtrayicon library for displaying X11 system tray icons
 */


#ifndef HAVE_TRAY_ICON_XCB_H
#define HAVE_TRAY_ICON_XCB_H

#include <xcb/xcb.h>
#include <xcb/xcb_image.h>

#define ICON_CNT_MAX 8 /* I believe this should be more than enough */
#define DEFAULT_FONT "fixed"

typedef struct x_struct x_t;

typedef enum {
	XTI_AT_CURSOR,
	XTI_GTK2_LIKE,
	XTI_GTK3_LIKE
	} xti_popup_position;

typedef enum {
	XTI_TRAY_ICON = 0,
	XTI_TOOLTIP = 1,
	XTI_MENU = 2,
	XTI_WIN_NO = 3
	} xti_window;

xcb_connection_t *
xti_get_connection (x_t *x);

xcb_screen_t *
xti_get_screen (x_t *x);

int
xti_get_screen_number (x_t *x);

/**
 * Get color depth of tray icon's window.
 *
 * @param x x_t struct returned by xti_initialize_tray_icon()
 *
 * @return return color depth or 0 on error
 */
int
xti_get_depth (x_t *x);

/**
 * Get window id of tray icon's window.
 *
 * @param x x_t struct returned by xti_initialize_tray_icon()
 *
 * @return return window id or 0 on error
 */
xcb_window_t
xti_get_tray_icon_window (x_t *x);

/**
 * Get current size of window.
 *
 * @param connection xcb connection
 * @param window tray icon's window id
 * @param width pointer to memory location where width will be written
 * @param height pointer to memory location where height will be written
 *
 * @return return 0 on success -1 on error
 */
int
xti_get_window_size (
		xcb_connection_t	*connection,
		xcb_window_t		 window,
		int			*width,
		int			*height);

/**
 * Initialize tray icon.
 * Allocate memory, create and dock window.
 *
 * @param background background color in "#RRGGBB" format or NULL
 * @param timeout try to find system tray area for this many seconds
 * @param progname set window name and class to this
 *
 * @return allocated x_t struct or NULL on error
 */
x_t *
xti_initialize_tray_icon (
		char		 *background,
		unsigned int	  timeout,
		char		 *progname);

/**
 * Draw some image on tray icon window.
 * The system tray icon must be initialized first. This will also cache
 * the supplied image.
 *
 * @param x x_t struct returned by xti_initialize_tray_icon()
 * @param cur intended cache position, must be unused
 * @param image image data suitable for X server
 * @param width width of image
 * @param height height of image
 *
 * @return 0 on success, -1 on error
 */
int
xti_draw_icon_from_data (
		x_t		*x,
		uint8_t		 cur,
		unsigned char	*image,
		int		 width,
		int		 height);

/**
 * Draw some image on tray icon window from xcb_image_t type.
 * The system tray icon must be initialized first. This will also cache
 * the supplied image.
 *
 * @param x x_t struct returned by xti_initialize_tray_icon()
 * @param cur intended cache position, must be unused
 * @param ximage xcb_image_t type image suitable for X server
 *
 * @return 0 on success, -1 on error
 */
int
xti_draw_icon_from_xicon (
		x_t		*x,
		uint8_t		 cur,
		xcb_image_t	*ximage);

/**
 * Draw cached image on tray icon window.
 *
 * @param x x_t struct returned by xti_initialize_tray_icon()
 * @param cur draw image at this cache position to system tray icon
 *
 * @return 0 on success, -1 on error
 */
int
xti_draw_icon_from_cache (x_t *x, uint8_t cur);

/**
 * Remove an image from chache.
 *
 * @param x x_t struct returned by xti_initialize_tray_icon()
 * @param cur remove image at this cache position
 */
void
xti_remove_icon_from_cache (x_t *x, uint8_t cur);

/**
 * Event handler for expose events.
 * This should be run for every expose event to recreate pixmap,
 * if necessary (on window size changes), or redraw exposed areas.
 *
 * @param x x_t struct returned by xti_initialize_tray_icon()
 * @param cur redraw image at this cache position to system tray icon
 * @param ev xcb expose event with details of exposed regions
 *
 * @return 0 on redraw exposed regions and 1 if pixmap is recreated,
 * in this case cache is cleared and a new image of the correct size
 * should be drawn on system tray icon
 */
int
xti_expose_event_handler (x_t *x, uint8_t cur, xcb_expose_event_t *ev);

/**
 * Request system tray icon to be docked.
 * When system tray changes, for example by a new panel, the tray icon
 * gets unmapped and xti_request_dock() or xti_request_dock_timeout()
 * should be called to request docking the icon to the new system tray.
 * This function tries docking only once.
 * xti_draw_icon_from_cache() should be called on successful docking to
 * actually display an icon.
 *
 * @param connection xcb connection
 * @param notification_icon tray icon's window id
 * @param screen number of current screen
 *
 * @return 1 on success and 0 on failure
 */
int
xti_request_dock (
		xcb_connection_t *connection,
		xcb_window_t      notification_icon,
		int               screen);

/**
 * Request system tray icon to be docked periodically.
 * When system tray changes, for example by a new panel, the tray icon
 * gets unmapped and xti_request_dock() or xti_request_dock_timeout()
 * should be called to request docking the icon to the new system tray.
 * This function tries docking until successful or timeout seconds run out.
 * xti_draw_icon_from_cache() should be called on successful docking to
 * actually display an icon.
 *
 * @param connection xcb connection
 * @param notification_icon tray icon's window id
 * @param screen number of current screen
 * @param time keep trying docking for this number of seconds
 *
 * @return 1 on success and 0 on failure
 */
int
xti_request_dock_timeout (
		xcb_connection_t *connection,
		xcb_window_t      notification_icon,
		int               screen,
		unsigned int      time);

/**
 * Close connection to X server and free allocated memory.
 *
 * @param x x_t struct returned by xti_initialize_tray_icon()
 */
void
xti_free_and_close_tray_icon (x_t *x);

/**
 * Create an empty tooltip. No text is set. Not shown.
 * Tray icon must be initialized first.
 *
 * @param x x_t struct returned by xti_initialize_tray_icon()
 * @param background background color in "#RRGGBB" format or NULL
 * @param foreground foreground color in "#RRGGBB" format or NULL
 *
 * @return return 0 on success -1 on error
 */
int
xti_create_tooltip (
		x_t	*x,
		char	*background,
		char	*foreground);

/**
 * Set text on tooltip. Tooltip must be created first.
 * Tooltip should be created only once.
 *
 * @param x x_t struct returned by xti_initialize_tray_icon()
 * @param text tooltip text
 *
 * @return return 0 on success -1 on error
 */
int
xti_tooltip_set_text (x_t *x, char *text);

#endif

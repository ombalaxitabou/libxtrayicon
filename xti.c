#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> /* sleep () */
#include <limits.h>

#include <xcb/xcb.h>
#include <xcb/xcb_image.h>
#include <xcb/xcb_icccm.h>

#include "xti.h"

#define ICON_SIZE 22 /* only used for window creation */
#define BUFFER_MAX 1000

#define MAXI(a, b) ((a > b) ? (a) : (b))
#define MINI(a, b) ((a < b) ? (a) : (b))

struct win_struct {
	xcb_window_t	  id;			/* window		*/
	int		  width;		/* window width		*/
	int		  height;		/* window height	*/
	int		  depth;		/* window color depth	*/
	xcb_gcontext_t	  gc;			/* graphic context	*/
	};

typedef struct win_struct win_t;

struct x_struct {
	xcb_connection_t *c;			/* connection		*/
	xcb_screen_t	 *s;			/* screen		*/
	int		  sn;			/* screen number	*/
	win_t		 *win[XTI_WIN_NO];	/* windows		*/

	xcb_image_t	 *xim[ICON_CNT_MAX];	/* image		*/
	int		  xim_w[ICON_CNT_MAX];	/* image width		*/
	int		  xim_h[ICON_CNT_MAX];	/* image height		*/
	xcb_pixmap_t	  pm;			/* pixmap		*/
	xcb_gcontext_t	  gc;			/* graphic context	*/
	xcb_colormap_t	  cmap;			/* colormap		*/
	};

typedef struct x_struct x_t;

xcb_connection_t *
xti_get_connection (x_t *x)
{
	return x->c;
}

xcb_screen_t *
xti_get_screen (x_t *x)
{
	return x->s;
}

int
xti_get_screen_number (x_t *x)
{
	return x->sn;
}

xcb_window_t
xti_get_tray_icon_window (x_t *x)
{
	if (!x->win[XTI_TRAY_ICON])
	{
		return 0;
	}

	return x->win[XTI_TRAY_ICON]->id;
}

int
xti_get_depth (x_t *x)
{
	if (!x->win[XTI_TRAY_ICON])
	{
		return 0;
	}

	return x->win[XTI_TRAY_ICON]->depth;
}

static xcb_atom_t
get_atom (xcb_connection_t *connection, const char *atom_name)
{
	xcb_intern_atom_cookie_t atom_cookie;
	xcb_intern_atom_reply_t *atom_reply;
	xcb_generic_error_t *error;
	xcb_atom_t atom;

	atom_cookie = xcb_intern_atom (
			connection,
			1, /* only if exists */
			strlen (atom_name),
			atom_name);
	atom_reply = xcb_intern_atom_reply (
			connection,
			atom_cookie,
			&error);
	if ((!atom_reply) || (error))
	{
		fprintf (stderr, "Could not get %s atom\n", atom_name);
		return 0;
	}

	atom = atom_reply->atom;
	free (atom_reply);

	return atom;
}

static xcb_colormap_t
create_colormap (xcb_connection_t *conn, xcb_window_t parent, xcb_visualid_t id)
{
	xcb_colormap_t colormap = 0;
	xcb_void_cookie_t ck;
	xcb_generic_error_t *error;

	colormap = xcb_generate_id (conn);
	ck = xcb_create_colormap_checked (
			conn,
			XCB_COLORMAP_ALLOC_NONE,
			colormap,
			parent,
			id);

	error = xcb_request_check (conn, ck);
	if (error)
	{
		fprintf (stderr, "Could not create color map. Error: %d\n", error->error_code);
		free (error);
		return 0;
	}

	return colormap;
}

static uint8_t
get_depth_of_visual (xcb_screen_t *screen, xcb_visualid_t id)
{
	xcb_depth_iterator_t		 depth_iter;
	xcb_visualtype_iterator_t	 visual_iter;
	xcb_visualtype_t		*visual_type;

	depth_iter = xcb_screen_allowed_depths_iterator (screen);
	while (depth_iter.rem)
	{
		visual_iter = xcb_depth_visuals_iterator (depth_iter.data);
		while (visual_iter.rem)
		{
			visual_type = visual_iter.data;

			if (id == visual_type->visual_id)
			{
				return depth_iter.data->depth;
			}

			xcb_visualtype_next (&visual_iter);
		}

		xcb_depth_next (&depth_iter);
	}

	return 0;
}

static xcb_visualid_t
get_visual_by_depth (xcb_screen_t *screen, uint8_t depth)
{
	xcb_depth_iterator_t		 depth_iter;
	xcb_visualtype_iterator_t	 visual_iter;
	xcb_visualtype_t		*visual_type;

	for (
	depth_iter = xcb_screen_allowed_depths_iterator (screen);
	depth_iter.rem;
	xcb_depth_next (&depth_iter))
	{
		if (depth_iter.data->depth != depth)
			continue;

		visual_iter = xcb_depth_visuals_iterator (depth_iter.data);

		visual_type = visual_iter.data;

		return visual_type->visual_id;
	}

	return 0;
}

static xcb_visualid_t
get_visual_of_window (xcb_connection_t *conn, xcb_window_t win)
{
	xcb_get_window_attributes_cookie_t attrib;
	xcb_get_window_attributes_reply_t *attrib_reply;
	xcb_visualid_t vis = 0;

	attrib = xcb_get_window_attributes (conn, win);
	attrib_reply = xcb_get_window_attributes_reply (conn, attrib, NULL);

	if (attrib_reply)
	{
		vis = attrib_reply->visual;
		free (attrib_reply);
	}

	return vis;
}

static xcb_visualid_t
get_system_tray_default_visual (xcb_connection_t *conn, xcb_window_t parent)
{
	xcb_atom_t visual_atom;
	xcb_visualid_t visual;
	xcb_get_property_cookie_t property;
	xcb_get_property_reply_t *property_reply;

	visual_atom = get_atom (conn, "_NET_SYSTEM_TRAY_VISUAL");
	if (!visual_atom)
	{
		fprintf (stderr, "Could not get system tray visual\n");
		return 0;
	}

	property = xcb_get_property (
			conn,
			0, /* do not delete */
			parent,
			visual_atom,
			XCB_ATOM_VISUALID,
			0,
			1);

	property_reply = xcb_get_property_reply (
			conn,
			property,
			NULL);
	if ((!property_reply)
	|| (property_reply->type != XCB_ATOM_VISUALID)
	|| (property_reply->bytes_after > 0)
	|| (property_reply->format != 32))
	{
		fprintf (stderr, "Could not get visual atom value\n");
		return 0;
	}

	visual = *(xcb_visualid_t *)xcb_get_property_value (property_reply);

	free (property_reply);

	return visual;
}

static xcb_font_t
open_font (x_t *x, char *font_name)
{
	xcb_font_t font;
	xcb_void_cookie_t ck;
	xcb_generic_error_t *error;

	font = xcb_generate_id (x->c);

	ck = xcb_open_font (x->c, font, strlen(font_name), font_name);

	error = xcb_request_check (x->c, ck);
	if (error)
	{
		fprintf (
				stderr,
				"Could not open font. Error: %d\n",
				error->error_code);
		free (error);
		return 0;
	}

	return font;
}

static void
close_font (x_t *x, xcb_font_t font)
{
	xcb_void_cookie_t ck;
	xcb_generic_error_t *error;

	/* Does this needs to be checked? */
	ck = xcb_close_font_checked (x->c, font);

	error = xcb_request_check (x->c, ck);
	if (error)
	{
		fprintf (
				stderr,
				"Could not close font. Error: %d\n",
				error->error_code);
		free (error);
		return;
	}
}

static uint32_t
hex_to_dec_6pos (char *hxstr)
{
	uint32_t	retval = 0;
	int		i;

	/* convert only the first 6 chars */
	for (i = 0; i < 6; i++, hxstr++)
	{
		if ((UINT32_MAX / 16 < retval)      /* overflow                    */
		|| ((*hxstr < 48 || *hxstr > 57)    /* not in the range of 0 ... 9 */
		&&  (*hxstr < 65 || *hxstr > 70)    /* not in A ... F              */
		&&  (*hxstr < 97 || *hxstr > 102))) /* not in a ... f              */
			return 0;

		retval *= 16;

		if (*hxstr > 47 || *hxstr < 58)
		{
			retval += *hxstr - 48;
		}
		else if (*hxstr > 64 || *hxstr < 71)
		{
			retval += *hxstr - 65 + 10;
		}
		else if (*hxstr > 96 || *hxstr < 103)
		{
			retval += *hxstr - 97 + 10;
		}
	}

	return retval;
}

static int
set_window_graphic_context (x_t *x, xti_window num, char *fg, char *bg, char *font_name)
{
	xcb_font_t font;
	uint32_t values[3];
	xcb_gcontext_t gc;
	uint32_t mask;
	xcb_void_cookie_t ck;
	xcb_generic_error_t *error;
	win_t *w = x->win[num];

	if(!w)
	{
		return -1;
	}

	mask = XCB_GC_FOREGROUND | XCB_GC_BACKGROUND | XCB_GC_FONT;

	if (fg)
	{
		values[0] = hex_to_dec_6pos (fg + 1);
	}
	else
	{
		values[0] = x->s->white_pixel;
	}

	if (bg)
	{
		values[1] = hex_to_dec_6pos (bg + 1);
	}
	else
	{
		values[1] = x->s->black_pixel;
	}

	if (font_name)
	{
		font = open_font (x, font_name);
	}
	else
	{
		font = open_font (x, DEFAULT_FONT);
	}

	values[2] = font;

	gc = xcb_generate_id (x->c);
	ck = xcb_create_gc_checked (x->c, gc, w->id, mask, values);

	error = xcb_request_check (x->c, ck);
	if (error)
	{
		fprintf (
				stderr,
				"Could not create graphic context for window. Error: %d\n",
				error->error_code);
		free (error);
		return -1;
	}

	w->gc = gc;

	close_font(x, font);

	return 0;
}

static xcb_window_t
create_window_fallback (
		x_t		*x,
		xti_window	 window_number)
{
	uint32_t	mask = 0;
	uint32_t	values[3];

	xcb_void_cookie_t ck;
	xcb_generic_error_t *error;

	/* Ask for a new window Id if we don't already have one */
	if (!x->win[window_number]->id)
	{
		x->win[window_number]->id = xcb_generate_id (x->c);
	}

	mask = XCB_CW_BACK_PIXEL | XCB_CW_BORDER_PIXEL;
	values[0] = x->s->black_pixel;
	values[1] = values[0];

	mask |= XCB_CW_EVENT_MASK;
	values[2] =	XCB_EVENT_MASK_STRUCTURE_NOTIFY	|
			XCB_EVENT_MASK_EXPOSURE		|
			XCB_EVENT_MASK_BUTTON_PRESS;

	/* Create the window */
	ck = xcb_create_window_checked (
			x->c,				/* Connection		*/
			x->s->root_depth,		/* depth		*/
			x->win[window_number]->id,	/* window Id		*/
			x->s->root,			/* parent window	*/
			0, 0,				/* x, y			*/
			ICON_SIZE, ICON_SIZE,		/* width, height	*/
			0,				/* border_width		*/
			XCB_WINDOW_CLASS_INPUT_OUTPUT,	/* class		*/
			x->s->root_visual,		/* visual		*/
			mask, values);			/* masks		*/

	/* Check for errors */
	error = xcb_request_check (x->c, ck);
	if (error)
	{
		fprintf (
				stderr,
				"Could not create tray icon. Error: %d\n",
				error->error_code);
		free (error);
		return 0;
	}

	x->win[window_number]->depth = x->s->root_depth;

	return x->win[window_number]->id;
}

static xcb_window_t
create_window (
		x_t		*x,
		xti_window	 window_number,
		xcb_window_t	 parent,
		xcb_visualid_t	 visual,
		uint8_t		 depth,
		char		*background)
{
	uint32_t	mask = 0;
	uint32_t	values[4];

	xcb_void_cookie_t ck;
	xcb_generic_error_t *error;

	if (depth && visual)
	{
		/* Check if supplied depth and visual match */
		if (depth != get_depth_of_visual (x->s, visual))
		{
			fprintf (stderr, "\
Could not create tray icon.\n\
Depth and visual does not match, set any of them to zero.\n");

			return 0;
		}
	}
	else if (visual)
	{
		depth = get_depth_of_visual (x->s, visual);
	}
	else if (depth)
	{
		visual = get_system_tray_default_visual (x->c, parent);
		if (!visual || depth != get_depth_of_visual (x->s, visual))
		{
			visual = get_visual_of_window (x->c, parent);
			if (depth != get_depth_of_visual (x->s, visual))
			{
				visual = get_visual_by_depth (x->s, depth);
			}
		}
	}
	else
	{
		visual = get_system_tray_default_visual (x->c, parent);

		if (visual)
		{
			depth = get_depth_of_visual (x->s, visual);
		}
		/*
		 * If the [_NET_SYSTEM_TRAY_VISUAL] property is not present,
		 * then tray icon windows should be created
		 * using the default visual of the screen.
		 *
		 * http://standards.freedesktop.org/systemtray-spec/systemtray-spec-latest.html#visuals
		 */
		else
		{
			visual = x->s->root_visual;
			depth = x->s->root_depth;
		}
	}

	/* Ask for our window's Id */
	x->win[window_number]->id = xcb_generate_id (x->c);

	if (background && *background)
	{
		mask = XCB_CW_BACK_PIXEL | XCB_CW_BORDER_PIXEL;
		values[0] = hex_to_dec_6pos (background + 1);
		values[1] = values[0];
	}
	else
	{
		/* Set back and border pixels to make X accept 32 bit visual */
		mask = XCB_CW_BACK_PIXEL | XCB_CW_BORDER_PIXEL;
		values[0] = x->s->black_pixel;
		values[1] = values[0];
	}

	mask |= XCB_CW_EVENT_MASK;
	values[2] =	XCB_EVENT_MASK_STRUCTURE_NOTIFY	|
			XCB_EVENT_MASK_EXPOSURE		|
			XCB_EVENT_MASK_BUTTON_PRESS;

	if (visual != get_visual_of_window (x->c, parent))
	{
		x->cmap = create_colormap (x->c, parent, visual);
		mask |= XCB_CW_COLORMAP;
		values[3] = x->cmap;
	}
	else if (!background || !*background)
	{	/*
		 * If no background color is provided by user
		 * and we use the same visual as parent window,
		 * then use pseudo transparent background.
		 */
		mask = XCB_CW_BACK_PIXMAP | XCB_CW_BORDER_PIXEL;
		values[0] = XCB_BACK_PIXMAP_PARENT_RELATIVE;
		values[1] = x->s->black_pixel;
	}

	/* Create the window */
	ck = xcb_create_window_checked (
			x->c,				/* Connection		*/
			depth,				/* depth		*/
			x->win[window_number]->id,	/* window Id		*/
			parent,				/* parent window	*/
			0, 0,				/* x, y			*/
			ICON_SIZE, ICON_SIZE,		/* width, height	*/
			0,				/* border_width		*/
			XCB_WINDOW_CLASS_INPUT_OUTPUT,	/* class		*/
			visual,				/* visual		*/
			mask, values);			/* masks		*/

	/* Check for errors */
	error = xcb_request_check (x->c, ck);
	if (error)
	{
		fprintf (
				stderr,
				"Could not create tray icon. Error: %d\n",
				error->error_code);
		free (error);
		/* Give it a last chance */
		return create_window_fallback (x, window_number);
	}

	x->win[window_number]->depth = depth;

	return x->win[window_number]->id;
}

static xcb_window_t
find_notification_area (xcb_connection_t *connection, int screen)
{
	char buffer[BUFFER_MAX];
	xcb_atom_t selection_atom;
	xcb_get_selection_owner_cookie_t cookie;
	xcb_get_selection_owner_reply_t *reply;
	xcb_window_t notification_area;
	xcb_generic_error_t *error;

	snprintf (buffer, BUFFER_MAX, "_NET_SYSTEM_TRAY_S%1d", screen);

	selection_atom = get_atom (connection, buffer);
	if (!selection_atom)
	{
		fprintf (stderr, "Could not find the notification area\n");
		return 0;
	}

	cookie = xcb_get_selection_owner (connection, selection_atom);
	reply = xcb_get_selection_owner_reply (
			connection,
			cookie,
			&error);
	if ((error) || (!reply))
	{
		fprintf (stderr, "Could not find the notification area\n");
		return 0;
	}

	notification_area = reply->owner;
	free (reply);

	return notification_area;
}

static xcb_window_t
find_notification_area_timeout (
		xcb_connection_t *connection,
		int screen,
		unsigned int time)
{
	xcb_window_t tray = find_notification_area (connection, screen);

	while (!tray)
	{
		if (!time)
			return 0;

		time--;

		sleep (1);

		tray = find_notification_area (connection, screen);
	}

	return tray;
}

static void
display_window_title (x_t *x, char *title)
{
	win_t *w = x->win[XTI_TRAY_ICON];

	if (!w)
	{
		return;
	}

	xcb_icccm_set_wm_name (
			x->c,
			w->id,
			XCB_ATOM_STRING,
			8,
			strlen (title),
			title);

	xcb_icccm_set_wm_icon_name (
			x->c,
			w->id,
			XCB_ATOM_STRING,
			8,
			strlen (title),
			title);

	xcb_icccm_set_wm_class (
			x->c,
			w->id,
			strlen (title),
			title);
}

static int
create_pixmap (x_t *x)
{
	win_t *w = x->win[XTI_TRAY_ICON];

	if (!w)
	{
		return -1;
	}

	/* Create backing pixmap */
	x->pm = xcb_generate_id (x->c);
	xcb_create_pixmap (x->c, w->depth, x->pm, w->id, w->width, w->height);

	/* Create graphic context */
	x->gc = xcb_generate_id (x->c);
	xcb_create_gc (x->c, x->gc, x->pm, 0, 0);

	return 0;
}

static void
destroy_pixmap_with_icon (x_t *x)
{
	int i;

	xcb_free_gc (x->c, x->gc);
	xcb_free_pixmap (x->c, x->pm);

	for (i = 0; i < ICON_CNT_MAX; i++)
	{
		if (x->xim[i] != NULL)
		{	/* this also frees image data */
			xcb_image_destroy (x->xim[i]);
			x->xim[i] = NULL;
		}
	}
}

/*
 * Get window size to draw icon.
 */
int
xti_get_window_size (
		xcb_connection_t	*conn,
		xcb_window_t		 win,
		int			*width,
		int			*height)
{
	xcb_get_geometry_cookie_t ck = xcb_get_geometry (conn, win);
	xcb_get_geometry_reply_t *geo = xcb_get_geometry_reply (conn, ck, NULL);
	if (!geo)
	{
		return -1;
	}

//	*depth = geo->depth;
	*width = geo->width;
	*height = geo->height;
	free (geo);

	return 0;
}

static int
get_window_size_internal (x_t *x, xti_window num)
{
	int width = 0, height = 0, ret;
	win_t *w = x->win[num];

	if (!w)
	{
		return -1;
	}

	ret = xti_get_window_size (x->c, w->id, &width, &height);

	w->width = width;
	w->height = height;

	return ret;
}

void
xti_remove_icon_from_cache (x_t *x, uint8_t cur)
{
	if (x->xim[cur] != NULL)
	{	/* this also frees image data */
		xcb_image_destroy (x->xim[cur]);
		x->xim[cur] = NULL;
	}
}

int
xti_draw_icon_from_cache (x_t *x, uint8_t cur)
{
	win_t *w = x->win[XTI_TRAY_ICON];

	if (!w || cur < 0 || cur > ICON_CNT_MAX || !x->xim[cur])
	{
		return -1;
	}

	xcb_clear_area (x->c, 0, w->id, 0, 0, w->width, w->height);

	xcb_image_put (x->c, x->pm, x->gc, x->xim[cur], 0, 0, 0);
	xcb_copy_area (x->c, x->pm, w->id, x->gc,
			0,
			0,
			(w->width - x->xim_w[cur]) / 2,
			(w->height - x->xim_h[cur]) / 2,
			x->xim_w[cur],
			x->xim_h[cur]);

	xcb_flush (x->c);

	return 0;
}

int
xti_draw_icon_from_data (
		x_t		*x,
		uint8_t		 cur,
		unsigned char	*image,
		int		 width,
		int		 height)
{
	win_t *w = x->win[XTI_TRAY_ICON];

	if (!w || cur < 0 || cur > ICON_CNT_MAX || x->xim[cur] || !image || width <= 0 || height <= 0)
	{
		return -1;
	}

	x->xim_w[cur] = width;
	x->xim_h[cur] = height;

	x->xim[cur] = xcb_image_create_native (
			x->c,
			x->xim_w[cur],
			x->xim_h[cur],
			XCB_IMAGE_FORMAT_Z_PIXMAP,
			w->depth,
			image,
			4 * width * height * sizeof (unsigned char),
			image);

	xti_draw_icon_from_cache (x, cur);

	return 0;
}

int
xti_draw_icon_from_xicon (
		x_t		*x,
		uint8_t		 cur,
		xcb_image_t	*ximage)
{
	if (cur < 0 || cur > ICON_CNT_MAX || x->xim[cur] || !ximage)
	{
		return -1;
	}

	x->xim[cur] = ximage;
	x->xim_w[cur] = ximage->width;
	x->xim_h[cur] = ximage->height;

	xti_draw_icon_from_cache (x, cur);

	return 0;
}

static inline void
redraw (x_t *x, win_t *w, uint8_t cur, xcb_expose_event_t *ev)
{
	int px, py, pw, ph, wx, wy;

	pw = x->xim_w[cur];
	ph = x->xim_h[cur];
	px = (w->width - pw) / 2;
	py = (w->height - ph) / 2;

	wx = MAXI(ev->x, px);
	wy = MAXI(ev->y, py);
	px = MAXI((ev->x - px), 0);
	py = MAXI((ev->y - py), 0);
	pw = MINI((pw - px), ev->width);
	ph = MINI((ph - py), ev->height);

	xcb_copy_area (x->c, x->pm, w->id, x->gc,
			px, py,
			wx, wy,
			pw, ph);

	xcb_flush (x->c);
}

int
xti_expose_event_handler (x_t *x, uint8_t cur, xcb_expose_event_t *ev)
{
	int w = 0;
	int h = 0;
	win_t *win = x->win[XTI_TRAY_ICON];

	if (!win)
	{
		/*
		 * This may never happen as we cannot receie event from
		 * non-existent window - in theory -, but check anyways.
		 * Thinking about race conditions.
		 */
		return 0;
	}

	w = win->width;
	h = win->height;

	/* find out new window size */
	get_window_size_internal (x, XTI_TRAY_ICON);

	if (w == win->width && h == win->height)
	{
		redraw (x, win, cur, ev);
		return 0;
	}

	destroy_pixmap_with_icon (x);
	create_pixmap (x);

	return 1;
}

int
xti_request_dock (
		xcb_connection_t *connection,
		xcb_window_t icon, /* notification icon to embed into area */
		int screen)
{
	xcb_atom_t netwm_systray_atom;
	xcb_window_t notification_area;
	xcb_client_message_event_t ev;

	netwm_systray_atom = get_atom (connection, "_NET_SYSTEM_TRAY_OPCODE");
	if (!netwm_systray_atom)
	{
		fprintf (stderr, "Could not embed icon into notification area\n");
		return 0;
	}

	notification_area = find_notification_area (connection, screen);
	if (!notification_area)
	{
		/*
		 * Could not find the notification area,
		 * but this maybe normal, so just return silently.
		 */
		return 0;
	}

	memset (&ev, 0, sizeof (ev));
	ev.response_type = XCB_CLIENT_MESSAGE;
	ev.format = 32;
	ev.sequence = 0;
	ev.window = notification_area;
	ev.type = netwm_systray_atom;
	ev.data.data32[0] = XCB_CURRENT_TIME;
	ev.data.data32[1] = 0;
	ev.data.data32[2] = icon;

	xcb_send_event (
			connection,
			0, /* propagate */
			notification_area,
			XCB_EVENT_MASK_NO_EVENT,
			(char *) &ev);

	return 1;
}

int
xti_request_dock_timeout (
		xcb_connection_t *connection,
		xcb_window_t icon, /* notification icon to embed into area */
		int screen,
		unsigned int time)
{
	while (!xti_request_dock (connection, icon, screen))
	{
		if (!time)
			return 0;

		time--;

		sleep (1);
	}

	return 1;
}

/* FIXME Maybe this could contain the event handler or some "main loop".
static int
display_tray_icon (x_t *x)
{
	return 0;
}
 */

void
free_window (x_t *x, xti_window num)
{
	if (!x->win[num])
	{
		return;
	}

	/* TODO window should be unmapped first */

	xcb_free_gc (x->c, x->win[num]->gc);

	free (x->win[num]);

	x->win[num] = NULL;
}

void
xti_free_and_close_tray_icon (x_t *x)
{
	int i;

	destroy_pixmap_with_icon (x);

	for (i = 0; i < XTI_WIN_NO; i++)
	{
		free_window (x, (xti_window) i);
	}

	if (x->cmap)
	{
		xcb_free_colormap (x->c, x->cmap);
	}

	if (!xcb_connection_has_error (x->c))
	{
		xcb_disconnect (x->c);
	}

	free (x);
}

x_t *
xti_initialize_tray_icon (
		char		 *background,
		unsigned int	  timeout,
		char		 *progname)
{
	x_t		*x     = NULL;
	win_t		*w     = NULL;
	xcb_window_t	 tray  = 0;
	xcb_window_t	 win   = 0;
	xcb_visualid_t	 viid  = 0;
	uint8_t		 depth = 0;

	x = memset (malloc (sizeof (x_t)), 0, sizeof (x_t));
	if (!x)
	{
		fprintf (stderr, "Could not allocate memory\n");
		return NULL;
	}

	w = memset (malloc (sizeof (win_t)), 0, sizeof (win_t));
	if (!w)
	{
		fprintf (stderr, "Could not allocate memory\n");
		return NULL;
	}

	x->win[XTI_TRAY_ICON] = w;

	if (background && *background)
	{
		depth = 24;
	}

	/*
	 * Open the connection to the X server.
	 * Use the DISPLAY environment variable as the default display name
	 */
	x->c = xcb_connect (NULL, &x->sn);
	if (xcb_connection_has_error (x->c))
	{
		fprintf (stderr, "Could not open display\n");
		xti_free_and_close_tray_icon (x);
		return NULL;
	}

	/* Get the first screen */
	x->s = xcb_setup_roots_iterator (xcb_get_setup (x->c)).data;

	/* Find notification area to be parent of notification icon window */
	tray = find_notification_area_timeout (x->c, x->sn, timeout);
	if (!tray)
	{
		xti_free_and_close_tray_icon (x);
		return NULL;
	}

	/* Create window with correct depth and visual */
	win = create_window (x, XTI_TRAY_ICON, tray, viid, depth, background);
	if (!win)
	{
		xti_free_and_close_tray_icon (x);
		return NULL;
	}

	/* Put icon in notification area */
	if (!xti_request_dock_timeout (x->c, x->win[XTI_TRAY_ICON]->id, x->sn, timeout))
	{
		return NULL;
	}

	/* Prepare to display an icon */
	get_window_size_internal (x, XTI_TRAY_ICON);
	create_pixmap (x);

	display_window_title (x, progname);

	return x;
}

int
xti_create_tooltip (
		x_t	*x,
		char	*background,
		char	*foreground)
{
	win_t *w = NULL;
	xcb_window_t win;

	w = memset (malloc (sizeof (win_t)), 0, sizeof (win_t));
	if (!w)
	{
		fprintf (stderr, "Could not allocate memory\n");
		return -1;
	}

	x->win[XTI_TOOLTIP] = w;

	/* Create window with correct depth and visual */
	win = create_window (
			x, XTI_TOOLTIP,
			x->s->root, x->s->root_visual, x->s->root_depth,
			background);
	if (!win)
	{
		xti_free_and_close_tray_icon (x);
		return -1;
	}

	return set_window_graphic_context (x, XTI_TOOLTIP, foreground, background, NULL);
}

int
xti_tooltip_set_text (x_t *x, char *text)
{
	xcb_window_t win;
	xcb_gcontext_t gc;
	xcb_void_cookie_t ck;
	xcb_generic_error_t *error;

	if (!x->win[XTI_TOOLTIP] || !x->win[XTI_TOOLTIP]->gc)
	{
		return -1;
	}

	win = x->win[XTI_TOOLTIP]->id;
	gc = x->win[XTI_TOOLTIP]->gc;

	ck = xcb_image_text_8_checked (
		x->c,
		strlen (text),
		win,
		gc,
		10, 10,
		text);

	error = xcb_request_check (x->c, ck);
	if (error)
	{
		fprintf (
				stderr,
				"Could not set tooltip text. Error: %d\n",
				error->error_code);
		free (error);
		return -1;
	}

	return 0;
}

int
xti_show_tooltip (x_t *x)
{
	xcb_void_cookie_t ck;
	xcb_generic_error_t *error;

	if (!x->win[XTI_TOOLTIP] || !x->win[XTI_TOOLTIP]->gc)
	{
		return -1;
	}

	ck = xcb_map_window_checked (x->c, x->win[XTI_TOOLTIP]->id);

	error = xcb_request_check (x->c, ck);
	if (error)
	{
		fprintf (
				stderr,
				"Could not show tooltip. Error: %d\n",
				error->error_code);
		free (error);
		return -1;
	}

	return 0;
}

int
xti_hide_tooltip (x_t *x)
{
	xcb_void_cookie_t ck;
	xcb_generic_error_t *error;

	if (!x->win[XTI_TOOLTIP] || !x->win[XTI_TOOLTIP]->gc)
	{
		return -1;
	}

	ck = xcb_unmap_window_checked (x->c, x->win[XTI_TOOLTIP]->id);

	error = xcb_request_check (x->c, ck);
	if (error)
	{
		fprintf (
				stderr,
				"Could not hide tooltip. Error: %d\n",
				error->error_code);
		free (error);
		return -1;
	}

	return 0;
}
